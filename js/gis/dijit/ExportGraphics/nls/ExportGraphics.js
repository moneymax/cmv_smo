// http://dojotoolkit.org/reference-guide/1.10/dojo/i18n.html
define({
    root: {
        buttonLabel: 'Export Drawing',
        noGraphicsMessage: {
            title: 'Export Drawing',
            message: 'There are no graphics to export.',
            level: 'warning'
        }
    }
});