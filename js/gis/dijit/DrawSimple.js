define([
    'dojo/_base/declare',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/i18n!./Draw/nls/resource',
    'dojo/text!./Draw/templates/Draw.html',
    'xstyle/css!./Draw/css/Draw.css',
    'xstyle/css!./Draw/css/adw-icons.css'
    // 'dojo/on',
    // 'dojo/domReady!'
    // 'dojo/dom',
], function (
    declare,
    _WidgetBase,
    _TemplatedMixin,
    _WidgetsInTemplateMixin,
    i18n,
    drawTemplate
) {

    // main draw dijit
    return declare([
            _WidgetBase,
            _TemplatedMixin, 
            _WidgetsInTemplateMixin
        ], {
        widgetsInTemplate: true,
        templateString: drawTemplate,
        i18n: i18n,
        postCreate: function () {
            alert('A');
        }
    });
});