define([
    'dojo/_base/declare',

    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',

    'dojo/_base/lang',
    'dojo/_base/Color',
    'dojo/i18n!./Draw/nls/resource',
    'esri/map',
    'dojo/on',
    'dojo/parser',
    'dijit/registry',
    'dojo/dom',
    'esri/toolbars/draw',
    'esri/layers/GraphicsLayer',
    'esri/graphic',
    'esri/config',
    'esri/renderers/SimpleRenderer',

    'dojo/text!./Draw/templates/Draw.html',
    'esri/renderers/UniqueValueRenderer',
    'esri/symbols/SimpleMarkerSymbol',
    'esri/symbols/SimpleLineSymbol',
    'esri/symbols/SimpleFillSymbol',
    'esri/symbols/PictureFillSymbol',
    'esri/symbols/CartographicLineSymbol',
    'esri/layers/FeatureLayer',
    'dojo/topic',
    'dojo/aspect',
    'dojo/dom-geometry',
    'jszip',
    'esri/tasks/GeometryService',

    'dijit/Dialog',

    'dojo/window',

    '//cdnjs.cloudflare.com/ajax/libs/proj4js/2.3.3/proj4.js',

    'xstyle/css!./Draw/css/Draw.css',
    'xstyle/css!./Draw/css/adw-icons.css',

    './Draw/widgets/JS2Shapefile',
    'dijit/form/Button',
    'dojo/domReady!'
], function (
    declare,
    _WidgetBase,
    _TemplatedMixin,
    _WidgetsInTemplateMixin,
    lang,
    Color,
    i18n,
    map,
    on,
    parser,
    registry,
    dom,
    Draw,
    GraphicsLayer,
    Graphic,
    esriConfig,
    SimpleRenderer,
    drawTemplate,
    UniqueValueRenderer,
    SimpleMarkerSymbol,
    SimpleLineSymbol,
    SimpleFillSymbol,
    PictureFillSymbol,
    CartographicLineSymbol,
    FeatureLayer,
    topic,
    aspect,
    domGeom,
    JSZip,
    Dialog,
    win,
    proj4,
    Button,
    tb,
    add)
{

    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
        widgetsInTemplate: true,
        templateString: drawTemplate,
        i18n: i18n,
        drawToolbar: null,
        info: null,
        mapClickMode: null,
        // in case this changes some day
        proj4BaseURL: 'http://spatialreference.org/',

        //  options are ESRI, EPSG and SR-ORG
        // See http://spatialreference.org/ for more information
        proj4Catalog: 'EPSG',

        // if desired, you can load a projection file from your server
        // instead of using one from spatialreference.org
        // i.e., http://server/projections/102642.js
        projCustomURL: null,

        postCreate: function () {
            this.inherited(arguments);
            //console.log(arguments);

            this.drawToolbar = new Draw(this.map);
            this.drawToolbar.on('draw-end', lang.hitch(this, 'onDrawToolbarDrawEnd'));

            this.exportShapeButton.on('click', lang.hitch(this, 'exportShapefile'));
            
            //this.initToolbar();

            this.createGraphicLayers();
            this.geometryService = esriConfig.defaults.geometryService;

            this.own(topic.subscribe('mapClickMode/currentSet', lang.hitch(this, 'setMapClickMode')));
            if (this.parentWidget && this.parentWidget.toggleable) {
                this.own(aspect.after(this.parentWidget, 'toggle', lang.hitch(this, function () {
                    this.onLayoutChange(this.parentWidget.open);
                })));
            };
            window.Proj4js = proj4;
        },

        createGraphicLayers: function () {
            // this.markerSymbol = new SimpleMarkerSymbol();
            // this.markerSymbol.setPath("M16,4.938c-7.732,0-14,4.701-14,10.5c0,1.981,0.741,3.833,2.016,5.414L2,25.272l5.613-1.44c2.339,1.316,5.237,2.106,8.387,2.106c7.732,0,14-4.701,14-10.5S23.732,4.938,16,4.938zM16.868,21.375h-1.969v-1.889h1.969V21.375zM16.772,18.094h-1.777l-0.176-8.083h2.113L16.772,18.094z");
            // this.markerSymbol.setColor(new Color("#00FFFF"));
            // this.lineSymbol = new CartographicLineSymbol(
            //     CartographicLineSymbol.STYLE_SOLID,
            //     new Color([255,0,0]), 5, 
            //     CartographicLineSymbol.CAP_ROUND,
            //     CartographicLineSymbol.JOIN_MITER, 5
            // );
            // this.fillSymbol = new PictureFillSymbol(
            //     new SimpleLineSymbol(
            //     SimpleLineSymbol.STYLE_SOLID,
            //     new Color('#000'), 
            //     1
            //     ), 
            //     42, 
            //     42
            // ); 
            
            this.pointSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_CIRCLE, 10, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color([255, 0, 0]), 1), new Color([255, 0, 0, 1.0]));
            this.polylineSymbol = new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASH, new Color([255, 0, 0]), 1);
            this.polygonSymbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_DASHDOT, new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.0]));
            this.pointGraphics = new GraphicsLayer({
                id: 'drawGraphics_point',
                title: 'Draw Graphics'
            });
            this.pointRenderer = new SimpleRenderer(this.pointSymbol);
            this.pointRenderer.label = 'User drawn points';
            this.pointRenderer.description = 'User drawn points';
            this.pointGraphics.setRenderer(this.pointRenderer);
            this.map.addLayer(this.pointGraphics);
            this.polylineGraphics = new GraphicsLayer({
                id: 'drawGraphics_line',
                title: 'Draw Graphics'
            });
            this.polylineRenderer = new SimpleRenderer(this.polylineSymbol);
            this.polylineRenderer.label = 'User drawn lines';
            this.polylineRenderer.description = 'User drawn lines';
            this.polylineGraphics.setRenderer(this.polylineRenderer);
            this.map.addLayer(this.polylineGraphics);

            this.polygonGraphics = new FeatureLayer({
                layerDefinition: {
                    geometryType: 'esriGeometryPolygon',
                    fields: [{
                        name: 'OBJECTID',
                        type: 'esriFieldTypeOID',
                        alias: 'OBJECTID',
                        domain: null,
                        editable: false,
                        nullable: false
                    }, {
                        name: 'ren',
                        type: 'esriFieldTypeInteger',
                        alias: 'ren',
                        domain: null,
                        editable: true,
                        nullable: false
                    }]
                },
                featureSet: null
            }, {
                id: 'drawGraphics_poly',
                title: 'Draw Graphics',
                mode: FeatureLayer.MODE_SNAPSHOT
            });
            this.polygonRenderer = new UniqueValueRenderer(new SimpleFillSymbol(), 'ren', null, null, ', ');
                this.polygonRenderer.addValue({
                    value: 1,
                    symbol: new SimpleFillSymbol({
                    color: [255,170,0,255],
                    outline: {
                        color: [255,170,0,255],
                        width: 1,
                        type: 'esriSLS',
                        style: 'esriSLSSolid'
                    },
                    type: 'esriSFS',
                    style: 'esriSFSForwardDiagonal'
                }),
                label: 'User drawn polygons',
                description: 'User drawn polygons'
            });
            this.polygonGraphics.setRenderer(this.polygonRenderer);
            this.map.addLayer(this.polygonGraphics);
        },
        drawPoint: function () {
            this.disconnectMapClick();
            this.drawToolbar.activate(Draw.POINT);
            this.drawModeTextNode.innerText = this.i18n.labels.point;
        },
        drawCircle: function () {
            this.disconnectMapClick();
            this.drawToolbar.activate(Draw.CIRCLE);
            this.drawModeTextNode.innerText = this.i18n.labels.circle;
        },
        drawLine: function () {
            this.disconnectMapClick();
            this.drawToolbar.activate(Draw.POLYLINE);
            this.drawModeTextNode.innerText = this.i18n.labels.polyline;
        },
        drawFreehandLine: function () {
            this.disconnectMapClick();
            this.drawToolbar.activate(Draw.FREEHAND_POLYLINE);
            this.drawModeTextNode.innerText = this.i18n.labels.freehandPolyline;
        },
        drawPolygon: function () {
            this.disconnectMapClick();
            this.drawToolbar.activate(Draw.POLYGON);
            this.drawModeTextNode.innerText = this.i18n.labels.polygon;
        },
        drawFreehandPolygon: function () {
            this.disconnectMapClick();
            this.drawToolbar.activate(Draw.FREEHAND_POLYGON);
            this.drawModeTextNode.innerText = this.i18n.labels.freehandPolygon;
        },
        disconnectMapClick: function () {
            topic.publish('mapClickMode/setCurrent', 'draw');
            this.enableStopButtons();
        },
        connectMapClick: function () {
            topic.publish('mapClickMode/setDefault');
            this.disableStopButtons();
        },
        onDrawToolbarDrawEnd: function (evt) {
            this.drawToolbar.deactivate();
            this.map.enableMapNavigation();
            this.drawModeTextNode.innerText = this.i18n.labels.currentDrawModeNone;

            var symbol;
            // if ( evt.geometry.type === 'point') {
            //     symbol = new Graphic(evt.geometry);
            //     this.pointGraphics.add(symbol);
            //     symbol = this.markerSymbol;
            // } else if ( evt.geometry.type === 'polyline') {
            //     symbol = new Graphic(evt.geometry);
            //     this.polylineGraphics.add(symbol);
            //     symbol = this.lineSymbol;
            // } else if ( evt.geometry.type === 'polygon') {
            //     symbol = new Graphic(evt.geometry, null, {
            //         ren: 1
            //     });
            //     this.polygonGraphics.add(symbol);
            //     symbol = this.fillSymbol;
            // }
            switch (evt.geometry.type) {
                case 'point':
                    symbol = this.pointSymbol;
                    symbol = new Graphic(evt.geometry);
                    this.pointGraphics.add(symbol);
                    break;
                case 'polyline':
                    symbol = this.polylineSymbol;
                    symbol = new Graphic(evt.geometry);
                    this.polylineGraphics.add(symbol);
                    break;
                case 'polygon':
                    symbol = this.polygonSymbol;
                    symbol = new Graphic(evt.geometry, null, {
                    ren: 1
                    });
                    this.polygonGraphics.add(symbol);
                    break;
                default:
            }
            //this.connectMapClick();
            this.map.graphics.add(new Graphic(evt.geometry, symbol));
        },
        clearGraphics: function () {
            this.endDrawing();
            this.connectMapClick();
            this.drawModeTextNode.innerText = 'None';
        },
        stopDrawing: function () {
            this.drawToolbar.deactivate();
            this.drawModeTextNode.innerText = 'None';
            this.connectMapClick();
        },
        endDrawing: function () {
            this.pointGraphics.clear();
            this.polylineGraphics.clear();
            this.polygonGraphics.clear();
            this.drawToolbar.deactivate();
            this.disableStopButtons();
        },
        disableStopButtons: function () {
            this.stopDrawingButton.set( 'disabled', true );
            this.eraseDrawingButton.set( 'disabled', !this.noGraphics() );
        },
        enableStopButtons: function () {
            this.stopDrawingButton.set( 'disabled', false );
            this.eraseDrawingButton.set( 'disabled', !this.noGraphics() );
        },
        noGraphics: function () {

            if ( this.pointGraphics.graphics.length > 0 ) {
                return true;
            } else if ( this.polylineGraphics.graphics.length > 0 ) {
                return true;
            } else if ( this.polygonGraphics.graphics.length > 0 ) {
                return true;
            } else {
                return false;
            }
            return false;
        },
        onLayoutChange: function (open) {
            // end drawing on close of title pane
            if (!open) {
                if (this.mapClickMode === 'draw') {
                    topic.publish('mapClickMode/setDefault');
                }
            }
        },
        setMapClickMode: function (mode) {
            this.mapClickMode = mode;
        },

        exportShapefile: function () {

            var coordSystem = 'PROJCS["WGS_1984_Web_Mercator_Auxiliary_Sphere", GEOGCS["GCS_WGS_1984", DATUM["D_WGS_1984", SPHEROID["WGS_1984", 6378137.0, 298.257223563]], PRIMEM["Greenwich", 0.0], UNIT["Degree", 0.0174532925199433]], PROJECTION["Mercator_Auxiliary_Sphere"], PARAMETER["False_Easting", 0.0], PARAMETER["False_Northing", 0.0], PARAMETER["Central_Meridian", 0.0], PARAMETER["Standard_Parallel_1", 0.0], PARAMETER["Auxiliary_Sphere_Type", 0.0], UNIT["Meter", 1.0]]';
            var zip = new JSZip();

            if (this.map.graphics.graphics.length > 0) {
                var outputObject = window.JS2Shapefile.createShapeFiles(this.map.graphics.graphics, 'UTF8', coordSystem);

                for (var createdFile in outputObject) {
                    if (outputObject[createdFile]['successful']) {
                        for (var fileInShape in outputObject[createdFile]['shapefile']) {
                            zip.file(outputObject[createdFile]['shapefile'][fileInShape]['name'], outputObject[createdFile]['shapefile'][fileInShape]['blob']);
                        }
                    }
                }
                var that = this;
                zip.generateAsync({ type: "arraybuffer" })
                    .then(function (arraybuffer) {
                        that.downloadFile(arraybuffer, 'application/zip', 'DrawingShapefile.zip', true);
                    });
            }
        },
        //from https://github.com/tmcgee/cmv-widgets/blob/master/widgets/Export.js
        downloadFile: function (content, mimeType, fileName, useBlob) {
            mimeType = mimeType || 'application/octet-stream';
            var url;
            var dataURI = 'data:' + mimeType + ',' + content;
            var link = document.createElement('a');
            var blob = new Blob([content], {
                'type': mimeType
            });

            // feature detection
            if (typeof (link.download) !== 'undefined') {
                // Browsers that support HTML5 download attribute
                if (useBlob) {
                    url = window.URL.createObjectURL(blob);
                } else {
                    url = dataURI;
                }
                link.setAttribute('href', url);
                link.setAttribute('download', fileName);
                link.style = 'visibility:hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
                return null;

                //feature detection using IE10+ routine
            } else if (navigator.msSaveOrOpenBlob) {
                return navigator.msSaveOrOpenBlob(blob, fileName);
            }

            // catch all. for which browsers?
            window.open(dataURI);
            window.focus();
            return null;
        }

    });
});